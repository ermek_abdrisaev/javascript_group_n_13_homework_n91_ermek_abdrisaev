const express = require('express');
const cors = require('cors');
const { nanoid } = require('nanoid');
const app = express();

require('express-ws')(app);

const port = 8000;

app.use(cors());

const activeConnections = {};
const savedCoords = [];

app.ws('/drawing', (ws, req) =>{
  const id = nanoid();
  console.log('client connected! id=', id);
  activeConnections[id] = ws;

  ws.send(JSON.stringify({
    type: 'PREV_DOT',
    coordinates: savedCoords,
  }));

  ws.on('message', msg => {
    const decodedMessage = JSON.parse(msg);
    switch(decodedMessage.type){
      case 'SEND_DOT':
        Object.keys(activeConnections).forEach(id =>{
          const conn = activeConnections[id];
          savedCoords.push(decodedMessage.coordinates)
          conn.send(JSON.stringify({
            type: 'NEW_DOT',
            dotCoordinates: decodedMessage.coordinates
          }))
        })
        break;
      default:
        console.log('Unknown type', decodedMessage.type);
    }
  })
})

app.listen(port, () =>{
  console.log(`Server on ${port} port!`);
});