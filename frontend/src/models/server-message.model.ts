import { Dot } from './dot.model';

export interface ServerMessage {
  type: string,
  coordinates: Dot[],
  dotCoordinates: Dot,
}
