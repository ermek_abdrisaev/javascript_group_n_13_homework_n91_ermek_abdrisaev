import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { ServerMessage } from '../models/server-message.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnDestroy, AfterViewInit {
  ws!: WebSocket;
  @ViewChild('canvas') canvas!: ElementRef;
  @ViewChild('favcolor') favcolor!: NgForm;


  ngAfterViewInit() {
    const canvas: HTMLCanvasElement  = this.canvas.nativeElement

    this.ws = new WebSocket('ws://localhost:8000/drawing');
    this.ws.onclose = () => console.log('ws closed');

    this.ws.onmessage = event => {
      const decodedMessage: ServerMessage = JSON.parse(event.data);

      if(decodedMessage.type === 'PREV_DOT'){
        decodedMessage.coordinates.forEach(c => {
          this.drawDot(c.x, c.y);
        });
      }

      if(decodedMessage.type === 'NEW_DOT'){
        const {x, y} = decodedMessage.dotCoordinates;
        this.drawDot(x, y)
      }

    }
  }

  ngOnDestroy(): void {
    this.ws.close();
  }

  drawDot(x: number, y: number){
    const canvas: HTMLCanvasElement = this.canvas.nativeElement;
    const ctx = canvas.getContext("2d")!;

    ctx.fillRect(x - 2.5, y - 2.5,5,5);
    ctx.fillStyle = this.favcolor.value;
    ctx.lineWidth = 6.5;
  }

  onCanvasClick(event: MouseEvent){
    const x = event.offsetX;
    const y = event.offsetY;
    this.ws.send(JSON.stringify({
      type: 'SEND_DOT',
      coordinates: {x, y},
    }))
  }


}
